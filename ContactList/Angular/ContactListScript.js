﻿var app = angular.module('myApp', []);




//Declaring service for search
app.service('ngservice', function ($http) {
    //get All Contacts
    this.getAllContacts = function () {
        return $http.get('/Home/GetAllContacts');
    };

    //bookmarked
    this.filterBookmarked = function () {
        return $http.get('/Home/GetBookmarked');
    };

    //get filtered contacts
    this.getFilteredContacts = function (filter) {
        if (filter.lenght == 0) {
            return $http.get('/Home/GetAllContacts');
        } else {
            return $http.get('/Home/SearchContacts/', { params: { "keyword": filter } });
        }
    };

    this.prepareForAdd = function () {
        return $http.get('/Home/PrepareforAdd');
    }

    // Manage bookmark
    this.manageBookmark = function (id) {
        return $http.get('/Home/ManageBookmark', { params: { "contactId": id } });
    };

    //delete contact
    this.delete = function (id) {
        return $http.get('/Home/DeleteContact', { params: { "contactId": id } });
    };

    this.permanentdelete = function (id) {
        return $http.get('/Home/PermanentDelete', { params: { "ContactId": id } });
    };

    this.getContactById = function (id) {
        return $http.get('/Home/GetContactById', { params: { "contactId": id } });
    };

    this.getLostContacts = function () {
        return $http.get('/Home/GetDeleted');
    };

    this.returnLostContacts = function (id) {
        return $http.get('/Home/ReturnDeleted', { params: { "ContactId": id } });
    };


});

//Controller - myCntrl
app.controller('myCntrl', function ($scope, $http, ngservice, $location) {
    $scope.filterValue = "";
    $scope.divContact = false;
    $scope.editDivShow = false;
    $scope.getDeletedDiv = false;
    $scope.divContactDetails = false;
    $scope.divContactStart = true;
    $scope.contactNotFound = false;
    $scope.contactFound = false;
    $scope.DetailsReadOnly = true;
    //$scope.startView = true;
    //$scope.detailsView = false;

    $scope.phoneNumbr = /^\+?\d{2}[- ]?\d{3}[- ]?\d{5}$/;

    $scope.$watch(function () {
        return location.hash
    }, function (value) {
        if (value) {
            var id = decodeURIComponent(value).split('/')[1];
            $scope.test = value;
            ngservice.getContactById(id).then(function (response) {
                $scope.divContactStart = false;
                $scope.divContactDetails = true;
                if (response.data === "notFound") {
                    $scope.contactNotFound = true;
                    $scope.contactFound = false;
                }
                else {
                    $scope.contactFound = true;
                    $scope.contactNotFound = false;
                    $scope.contactDetailsModel = JSON.parse(response.data);
                }
            });
            
        }
    });
    


    //$scope.addNewMailValue = "";
    //$scope.dataModel.Emails = "";
    //$scope.addNewMailValue = "";
    //$scope.editModel = "";
    //$scope.addNewNum = "";
    $scope.mailovi = "";



    loadContacts();

    function loadContacts() {
        ngservice.getAllContacts().then(function (response) {
            $scope.Kontakti = JSON.parse(response.data);
        });
        ngservice.filterBookmarked().then(function (resp) {
            $scope.BookmarkedContacts = JSON.parse(resp.data);
        })

    }

    $scope.getFilteredContacts = function () {
        ngservice.getFilteredContacts($scope.filterValue).then(function (response) {
            $scope.Kontakti = JSON.parse(response.data);
        });
    }

    $scope.changeBookmark = function (contactID) {
        ngservice.manageBookmark(contactID).then(function () {
            loadContacts();
        });

    }

    $scope.ReturnDeletedContacts = function () {
        $scope.getDeletedDiv = true;
        ngservice.getLostContacts().then(function (response) {
            $scope.lostContacts = JSON.parse(response.data);
        });
    }

    $scope.returnContact = function (ContactId) {
        ngservice.returnLostContacts(ContactId).then(function () {
            loadContacts();
            $scope.getDeletedDiv = false;
        });
    }


    //Add contact
    $scope.AddContact = function () {
        //debugger
        $http({
            method: 'POST',
            url: '/Home/AddNewContact',
            data: $scope.dataModel
        }).then(function () {
            loadContacts();
            ClearFileds();
            $scope.divContact = false;
        });
    }

    //Fill data for edit contact
    $scope.getContactById = function (contactID) {
        ngservice.getContactById(contactID).then(function (response) {
            $scope.editDivShow = true;
            $scope.editContact = JSON.parse(response.data);
        });
    }

    //Update contact
    $scope.UpdateContact = function () {
        //debugger
        $http({
            method: 'POST',
            url: '/Home/UpdateContact',
            data: $scope.editContact
        }).then(function () {
            loadContacts();
            $scope.editDivShow = false;
        });
    }



    //Delete contact
    $scope.DeleteContact = function (contactID) {
        ngservice.delete(contactID).then(function () {
            loadContacts();
        });
    }

    //Delete Details contact
    $scope.DeleteDetailsContact = function (contactID) {
        //$scope.takeId = contactID;
        ngservice.delete(contactID).then(function () {
            ngservice.getContactById(contactID).then(function (response) {
                $scope.contactDetailsModel = JSON.parse(response.data);
                $scope.DetailsReadOnly = true;
            })
        });        
        }
    //Return Details contact
    $scope.returnDetailsContact = function (ContactId) {
        ngservice.returnLostContacts(ContactId).then(function () {
            ngservice.getContactById(ContactId).then(function (response) {
                $scope.contactDetailsModel = JSON.parse(response.data);
                $scope.DetailsReadOnly = true;
            })
            
        });
    }


    //Permamnent Details delete
    $scope.permanentDetailsDelete = function(contactID){
        ngservice.permanentdelete(contactID).then(function () {
            loadContacts();
            $scope.getDeletedDiv = false;
            $scope.divContactDetails = false;
            $scope.divContactStart = true;
            $location.url('');
        });

    }

    //permanent 
    $scope.permanentDelete = function (contactID) {
        ngservice.permanentdelete(contactID).then(function () {
            loadContacts();
            $scope.getDeletedDiv = false;
        });

    }


    function ClearFileds() {
        $scope.dataModel = null;
    }
    $scope.Cancel = function () {
        $scope.divContact = false;
    };
    $scope.CancelEdit = function () {
        $scope.editDivShow = false;
    };

    $scope.AddContactDiv = function () {
        ngservice.prepareForAdd().then(function (response) {
            ClearFileds();
            $scope.divContact = true;
            $scope.dataModel = JSON.parse(response.data);
        });
    }

    $scope.AddEditDiv = function () {
        $scope.editDataModel = null;
        $scope.divEdit = true;
    }
    $scope.AddLostContactsDiv = function () {
        $scope.getDeletedDiv = true;
        ReturnDeletedContacts();
    }

    $scope.clearSearch = function () {
        $scope.filterValue = "";
        loadContacts();
    }
    $scope.CloseLost = function () {
        $scope.getDeletedDiv = false;
    }


    $scope.AddNewNumberToContact = function () {
        $scope.editContact.Numbers = $scope.editContact.Numbers.concat({ Id: null, Number: $scope.addNewNum });
        $scope.addNewNum = "";
    }

    $scope.deleteExistingNumber = function (x) {
        var index = $scope.editContact.Numbers.indexOf(x);
        $scope.editContact.Numbers.splice(index, 1);
    }


    $scope.AddNewEmailToContact = function () {
        $scope.editContact.Emails = $scope.editContact.Emails.concat({ Id: null, EmailAddress: $scope.addNewMail });
        $scope.addNewMail = "";
    }

    $scope.deleteExistingMail = function (x) {
        var index = $scope.editContact.Emails.indexOf(x);
        $scope.editContact.Emails.splice(index, 1);
    }


    $scope.AddNewTagToContact = function () {
        $scope.editContact.Tags = $scope.editContact.Tags.concat({ Id: null, TagName: $scope.addNewTag });
        $scope.addNewTag = "";
    }

    $scope.deleteExistingTag = function (x) {
        var index = $scope.editContact.Tags.indexOf(x);
        $scope.editContact.Tags.splice(index, 1);
    }


    //$scope.addNewContactEmails = function () {
    //    $scope.check = true;
    //    //$scope.dataModel.Emails = $scope.dataModel.Emails.push({ Id: null, EmailAddress: $scope.ajdeeee });
    //    $scope.dataModel.Emails.unshift($scope.daj);
    //   //dataModel.Emails.unshift($scope.ajdeeee);

    //    $scope.daj = "";
    //}

    //}

    $scope.AddNewNum = function () {        
        $scope.dataModel.Numbers =  $scope.dataModel.Numbers.concat($scope.numToAdd);
        $scope.numToAdd = "";
    }

    $scope.delNum = function (x) {
        var index = $scope.dataModel.Numbers.indexOf(x);
        $scope.dataModel.Numbers.splice(index, 1);
    }

    $scope.AddNewMail = function () {
        $scope.dataModel.Emails = $scope.dataModel.Emails.concat($scope.addMail);
        $scope.addMail = "";
    }

    $scope.delMail = function (x) {
        var index = $scope.dataModel.Emails.indexOf(x);
        $scope.dataModel.Emails.splice(index, 1);
    }

    $scope.AddNewTag = function () {
        $scope.dataModel.Tags = $scope.dataModel.Tags.concat($scope.addTag);
        $scope.addTag = "";
    }

    $scope.delTag = function (x) {
        var index = $scope.dataModel.Tags.indexOf(x);
        $scope.dataModel.Tags.splice(index, 1);
    }

    //Close Details
    $scope.BackToContacts = function () {        
            $scope.divContactStart = true;
            $scope.divContactDetails = false;
            loadContacts();   
            $location.url('');
    }


    $scope.EditDetailsMode = function () {
        $scope.DetailsReadOnly = false;
    }

    $scope.CancelDetailsEdit = function (id) {
        ngservice.getContactById(id).then(function (response) {	
            $scope.contactDetailsModel = JSON.parse(response.data);
            $scope.DetailsReadOnly = true;
        })
    }


    //Update Details contact
    $scope.UpdateDetailsContact = function (id) {
        //debugger
        $http({
            method: 'POST',
            url: '/Home/UpdateContact',
            data: $scope.contactDetailsModel            
        }).then(function () {
            ngservice.getContactById(id).then(function (response) {
                $scope.contactDetailsModel = JSON.parse(response.data);
                $scope.DetailsReadOnly = true;
            })
        });
    }

    


    //Details add/delete functions
    $scope.DetailsAddNewNum = function () {
        $scope.contactDetailsModel.Numbers = $scope.contactDetailsModel.Numbers.concat({ Id: null, Number: $scope.DetailsnumToAdd });
        $scope.DetailsnumToAdd = "";
    }

    $scope.DetailsdelNum = function (x) {
        var index = $scope.contactDetailsModel.Numbers.indexOf(x);
        $scope.contactDetailsModel.Numbers.splice(index, 1);
    }


    $scope.DetailsAddNewMail = function () {
        $scope.contactDetailsModel.Emails = $scope.contactDetailsModel.Emails.concat({ Id: null, EmailAddress: $scope.DetailsaddMail });
        $scope.DetailsaddMail = "";
    }

    $scope.DetailsdelMail = function (x) {
        var index = $scope.contactDetailsModel.Emails.indexOf(x);
        $scope.contactDetailsModel.Emails.splice(index, 1);
    }


    $scope.DetailsAddNewTag = function () {
        $scope.contactDetailsModel.Tags = $scope.contactDetailsModel.Tags.concat({ Id: null, TagName: $scope.DetailsaddTag });
        $scope.DetailsaddTag = "";
    }

    $scope.DetailsdelTag = function (x) {
        var index = $scope.contactDetailsModel.Tags.indexOf(x);
        $scope.contactDetailsModel.Tags.splice(index, 1);
    }



});