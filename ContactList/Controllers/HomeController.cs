﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ContactList.Models;
using System.Data.Entity;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;

namespace ContactList.Controllers
{
    public class HomeController : Controller
    {
        ContactsDb db = new ContactsDb();

        public ActionResult Index()
        {
            var model = db.Contacts.Include(c => c.Emails).Include(b => b.PhoneNumbers).Include(t => t.Tags).ToList();
            return View(model);
        }

        //Ispis svih kontakata
        public JsonResult GetAllContacts()
        {
            var contacts = db.Contacts.Include(c => c.Emails).Include(b => b.PhoneNumbers).Include(t => t.Tags)
                .Where(i => i.isDeleted == false)
                .ToList();

            string response = JsonConvert.SerializeObject(contacts, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        //Dohvat bookmark kontakata
        public JsonResult GetBookmarked()
        {
            var contacts = db.Contacts.Where(x => (x.IsBookmarked == true && x.isDeleted == false))
            .ToList();


            string response = JsonConvert.SerializeObject(contacts, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return Json(response, JsonRequestBehavior.AllowGet);

        }

        //Dohvat kontakata po kriteriju pretrage
        [Route("Home/SearchContacts/{keyword}")]
        public JsonResult SearchContacts(string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
            {
                Task.WaitAll(Task.Delay(1000));

                var filteredContacts = db.Contacts.Include(t => t.Tags)
                        .Where(i => (i.FirstName.StartsWith(keyword) || i.LastName.StartsWith(keyword)))
                        .Where(i => i.isDeleted == false)
                        .ToList();

                var hitedByTag = db.Contacts.Include(t => t.Tags)
                    .Where(x => (x.Tags.Any(r => r.TagName.StartsWith(keyword)) && x.isDeleted == false))
                    .ToList();

                filteredContacts.AddRange(hitedByTag);

                string response = JsonConvert.SerializeObject(filteredContacts, Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                return Json(response, JsonRequestBehavior.AllowGet);
            }
            return GetAllContacts();

        }




        //Dodavanje kontakta
        [HttpPost]
        public JsonResult AddNewContact(AddContactViewModel newContact)
        {

            Contact addContact = new Contact();

            addContact.FirstName = newContact.FirstName;
            addContact.LastName = newContact.LastName;
            addContact.City = newContact.City;
            addContact.Address = newContact.Address;
            addContact.IsBookmarked = newContact.IsBookmarked;
            addContact.Emails = new List<Email>();
            addContact.PhoneNumbers = new List<PhoneNumber>();
            addContact.Tags = new List<Tag>();

            //var allEmails = newContact.Emails.Where(x => !string.IsNullOrEmpty(x));
            //var allNumbers = newContact.Numbers.Where(x => !string.IsNullOrEmpty(x));
            //var allTags = newContact.Tags.Where(x => !string.IsNullOrEmpty(x));

            if (newContact.Emails != null)
            {
                var allEmails = newContact.Emails.Where(x => !string.IsNullOrEmpty(x));
                if (allEmails != null)
                {
                    //var addEmails = newContact.Emails;
                    var addEmails = allEmails.Select(x => x.ToLower()).Distinct().ToArray();
                    foreach (var mail in addEmails)
                    {
                        var add = mail;
                        addContact.Emails.Add(new Email { EmailAddress = add });
                    }
                }
            }

            if (newContact.Numbers != null)
            {
                var allNumbers = newContact.Numbers.Where(x => !string.IsNullOrEmpty(x));
                if (allNumbers != null)
                {
                    var addNumbers = allNumbers.Distinct().ToArray();
                    foreach (var number in addNumbers)
                    {
                        addContact.PhoneNumbers.Add(new PhoneNumber { Number = number });
                    }

                }
            }

            if (newContact.Tags != null)
            {
                var allTags = newContact.Tags.Where(x => !string.IsNullOrEmpty(x));
                if (allTags != null)
                {
                    var addTags = allTags.Select(x => x.ToLower()).Distinct().ToArray();

                    foreach (var tag in addTags)
                    {
                        var help = true;
                        foreach (var DBtag in db.Tags)
                        {
                            if (DBtag.TagName.Equals(tag))
                            {
                                addContact.Tags.Add(DBtag);
                                help = false;
                            }
                        }
                        if (help)
                        {
                            addContact.Tags.Add(new Tag { TagName = tag });
                        }
                    }

                }
            }


            db.Contacts.Add(addContact);

            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                var a = e;
            }

            string message = "Contact added succesfully !";
            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        // Izmjeni kontakt
        [HttpPost]
        public JsonResult UpdateContact(EditContactViewModel editContact)
        {

            Contact temp = new Contact();
            int id = editContact.Id;
            temp = db.Contacts.Find(id);

            List<Email> helpMails = new List<Email>();
            helpMails = temp.Emails;

            List<PhoneNumber> helpNum = new List<PhoneNumber>();
            helpNum = temp.PhoneNumbers;


            foreach (Email del in helpMails.ToList())
            {
                db.Emails.Remove(del);

            }

            if (editContact.Emails != null)
            {
                var addEmails = editContact.Emails.GroupBy(x => x.EmailAddress.ToLower()).Select(y => y.First());
                foreach (Email add in addEmails)
                {
                    temp.Emails.Add(add);

                }
            }

            foreach (PhoneNumber del in helpNum.ToList())
            {
                db.PhoneNumbers.Remove(del);

            }

            if (editContact.Numbers != null)
            {
                var addNumbers = editContact.Numbers.GroupBy(x => x.Number).Select(y => y.First());
                foreach (PhoneNumber add in addNumbers)
                {
                    temp.PhoneNumbers.Add(add);

                }
            }


            var tags = temp.Tags;
            var getEdtited = editContact.Tags;
            var temptgetEdtitedags = new List<Tag>();


            if (getEdtited != null)
            {
                var addTags = new List<Tag>();
                var remoweTags = new List<Tag>();
                var edited = getEdtited.GroupBy(x => x.TagName.ToLower()).Select(y => y.First());
                //nalazi se u Edited, ali ne u Tags dodaj ga
                foreach (var tag in edited)
                {
                    if (!(tags.Any(x => x.TagName.ToLower().Equals(tag.TagName.ToLower()))))
                    {
                        addTags.Add(tag);
                    }
                }
                //Ime se nalazi u Tags ali ne u Edited ttreba izbrisati
                foreach (var DBtag in tags)
                {
                    if (!(edited.Any(x => x.TagName.ToLower().Equals(DBtag.TagName.ToLower()))))
                    {
                        remoweTags.Add(DBtag);
                    }
                }
                // Idemo dodati one koje trebamo
                foreach (var addTag in addTags)
                {
                    var help = true;
                    foreach (var tagDB in db.Tags)
                    {
                        if (tagDB.TagName.Equals(addTag.TagName))
                        {
                            temp.Tags.Add(tagDB);
                            help = false;
                        }

                    }
                    if (help)
                    {
                        temp.Tags.Add(new Tag { TagName = addTag.TagName });
                    }

                }
                //Jos trebamo izbrisati
                foreach(var delete in remoweTags)
                {
                    temp.Tags.Remove(delete);
                }

            }
            else
            {
                foreach(var tag in tags.ToList())
                {
                    temp.Tags.Remove(tag);                                               

                }
            }

            

           


            temp.FirstName = editContact.FirstName;
            temp.LastName = editContact.LastName;
            temp.City = editContact.City;
            temp.Address = editContact.Address;
            temp.IsBookmarked = editContact.IsBookmarked;

            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                var a = e;
            }

            CheckTags();

            string message = "Contact added succesfully !";
            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        private void CheckTags()
        {
            foreach (var emptyTag in db.Tags)
            {
                if (emptyTag.Contacts.Count == 0)
                {
                    db.Tags.Remove(emptyTag);
                }
            }
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                var a = e;
            }

            
        }

        //Brisanje kontakta
        [Route("Home/DeleteContact/{ContactId}")]
        public JsonResult DeleteContact(int ContactId)
        {

            db.Contacts.Find(ContactId).isDeleted = true;


            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                var a = e;
            }

            string message = "Contact remowed ";
            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        //Dohvati obrisane
        public JsonResult GetDeleted()
        {
            var contacts = db.Contacts.Include(c => c.Emails).Include(b => b.PhoneNumbers).Include(t => t.Tags)
                .Where(i => i.isDeleted == true)
                .ToList();

            string response = JsonConvert.SerializeObject(contacts, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        //Vrati obrisani kontakt
        [Route("Home/ReturnDeleted/{ContactId}")]
        public JsonResult ReturnDeleted(int ContactId)
        {
            db.Contacts.Find(ContactId).isDeleted = false;

            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                var a = e;
            }

            string message = "Contact remowed ";
            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        // Obrisi iz baze
        [Route("Home/PermanentDelete/{ContactId}")]
        public JsonResult PermanentDelete(int ContactId)
        {
            var deleteContact = db.Contacts.Find(ContactId);


            var deleteMails = new List<Email>();
            foreach (var mail in deleteContact.Emails)
            {
                deleteMails.Add(mail);
            }

            db.Emails.RemoveRange(deleteMails);


            var deleteNumbers = new List<PhoneNumber>();
            foreach (var num in deleteContact.PhoneNumbers)
            {
                deleteNumbers.Add(num);
            }

            db.PhoneNumbers.RemoveRange(deleteNumbers);


            var deleteTags = new List<Tag>();
            foreach (var tag in deleteContact.Tags)
            {
                deleteTags.Add(tag);
            }

            foreach(var deleteTag in deleteTags)
            {
                deleteContact.Tags.Remove(deleteTag);
            }
                       

            db.Contacts.Remove(deleteContact);

            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                var a = e;
            }

                CheckTags();

                string message = "Contact remowed ";
            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        //Dodaj bookmark
        public JsonResult ManageBookmark(int contactId)
        {

            Contact channgeContact = new Contact();
            bool set = new bool();
            channgeContact = db.Contacts.Find(contactId);
            if (channgeContact.IsBookmarked == true)
            {
                set = false;
            }
            else
            {
                set = true;
            }

            db.Contacts.Find(contactId).IsBookmarked = set;

            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                var a = e;
            }

            string message = "Contact bookmarked succesfully !";
            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };


        }

        //Dohvat kontakta po ID-u
        public JsonResult GetContactById(int contactId)
        {
            Contact contact = new Contact();
            contact = db.Contacts.Find(contactId);

            if (contact != null)
            {
                EditContactViewModel temp = new EditContactViewModel();
                temp.Id = contact.Id;
                temp.FirstName = contact.FirstName;
                temp.LastName = contact.LastName;
                temp.City = contact.City;
                temp.Address = contact.Address;
                temp.IsBookmarked = contact.IsBookmarked;
                temp.IsDeleted = contact.isDeleted;
                temp.Numbers = contact.PhoneNumbers;
                temp.Emails = contact.Emails;
                temp.Tags = contact.Tags;

                string response = JsonConvert.SerializeObject(temp, Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string message = "notFound";
                return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

        }


        public JsonResult PrepareforAdd()
        {
            AddContactViewModel temp = new AddContactViewModel();
            List<string> help = new List<string>();
            temp.FirstName = "";
            temp.LastName = "";
            temp.City = "";
            temp.Address = "";
            temp.IsBookmarked = false;
            temp.Numbers = help;
            temp.Emails = help;
            temp.Tags = help;

            string response = JsonConvert.SerializeObject(temp, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (db != null)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}