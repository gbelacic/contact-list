namespace ContactList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        City = c.String(),
                        Address = c.String(),
                        Birthday = c.DateTime(nullable: false),
                        IsBookmarked = c.Boolean(nullable: false),
                        Email = c.String(),
                        PhoneNumber = c.String(),
                        TagsListing = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TagName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TagContacts",
                c => new
                    {
                        Tag_Id = c.Int(nullable: false),
                        Contact_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_Id, t.Contact_Id })
                .ForeignKey("dbo.Tags", t => t.Tag_Id, cascadeDelete: true)
                .ForeignKey("dbo.Contacts", t => t.Contact_Id, cascadeDelete: false)
                .Index(t => t.Tag_Id)
                .Index(t => t.Contact_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TagContacts", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.TagContacts", "Tag_Id", "dbo.Tags");
            DropIndex("dbo.TagContacts", new[] { "Contact_Id" });
            DropIndex("dbo.TagContacts", new[] { "Tag_Id" });
            DropTable("dbo.TagContacts");
            DropTable("dbo.Tags");
            DropTable("dbo.Contacts");
        }
    }
}
