namespace ContactList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test9 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Emails", new[] { "Contact_Id1" });
            DropIndex("dbo.PhoneNumbers", new[] { "Contact_Id1" });
            DropColumn("dbo.Emails", "Contact_Id");
            DropColumn("dbo.PhoneNumbers", "Contact_Id");
            RenameColumn(table: "dbo.Emails", name: "Contact_Id1", newName: "Contact_Id");
            RenameColumn(table: "dbo.PhoneNumbers", name: "Contact_Id1", newName: "Contact_Id");
            AlterColumn("dbo.Emails", "Contact_Id", c => c.Int());
            AlterColumn("dbo.PhoneNumbers", "Contact_Id", c => c.Int());
            CreateIndex("dbo.Emails", "Contact_Id");
            CreateIndex("dbo.PhoneNumbers", "Contact_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PhoneNumbers", new[] { "Contact_Id" });
            DropIndex("dbo.Emails", new[] { "Contact_Id" });
            AlterColumn("dbo.PhoneNumbers", "Contact_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Emails", "Contact_Id", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.PhoneNumbers", name: "Contact_Id", newName: "Contact_Id1");
            RenameColumn(table: "dbo.Emails", name: "Contact_Id", newName: "Contact_Id1");
            AddColumn("dbo.PhoneNumbers", "Contact_Id", c => c.Int(nullable: false));
            AddColumn("dbo.Emails", "Contact_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.PhoneNumbers", "Contact_Id1");
            CreateIndex("dbo.Emails", "Contact_Id1");
        }
    }
}
