namespace ContactList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test20 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Contacts", "Birthday");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contacts", "Birthday", c => c.DateTime(nullable: false));
        }
    }
}
