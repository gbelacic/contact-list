namespace ContactList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testfinal3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TagContacts", "Tag_Id", "dbo.Tags");
            DropForeignKey("dbo.TagContacts", "Contact_Id", "dbo.Contacts");
            AddForeignKey("dbo.TagContacts", "Tag_Id", "dbo.Tags", "Id");
            AddForeignKey("dbo.TagContacts", "Contact_Id", "dbo.Contacts", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TagContacts", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.TagContacts", "Tag_Id", "dbo.Tags");
            AddForeignKey("dbo.TagContacts", "Contact_Id", "dbo.Contacts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.TagContacts", "Tag_Id", "dbo.Tags", "Id", cascadeDelete: true);
        }
    }
}
