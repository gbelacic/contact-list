// <auto-generated />
namespace ContactList.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class testfinal4 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(testfinal4));
        
        string IMigrationMetadata.Id
        {
            get { return "201702251608200_testfinal4"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
