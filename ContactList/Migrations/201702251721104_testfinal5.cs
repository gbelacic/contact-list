namespace ContactList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testfinal5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contacts", "isDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contacts", "isDeleted");
        }
    }
}
