namespace ContactList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test71 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TagContacts", "Tag_Id", "dbo.Tags");
            DropForeignKey("dbo.TagContacts", "Contact_Id", "dbo.Contacts");
            AddForeignKey("dbo.TagContacts", "Tag_Id", "dbo.Tags", "Id", cascadeDelete: true);
            AddForeignKey("dbo.TagContacts", "Contact_Id", "dbo.Contacts", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TagContacts", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.TagContacts", "Tag_Id", "dbo.Tags");
            AddForeignKey("dbo.TagContacts", "Contact_Id", "dbo.Contacts", "Id");
            AddForeignKey("dbo.TagContacts", "Tag_Id", "dbo.Tags", "Id");
        }
    }
}
