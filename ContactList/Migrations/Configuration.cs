namespace ContactList.Migrations
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ContactList.Models.ContactsDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ContactList.Models.ContactsDb";
        }

        protected override void Seed(ContactList.Models.ContactsDb context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            Contact addContact = new Contact();
            addContact.FirstName = "Goran";
            addContact.LastName = "Belacic";
            addContact.City = "Zagreb";
            addContact.Address = "Jarunska 2";
            addContact.IsBookmarked = true;
            addContact.Emails = new List<Email>();
            addContact.PhoneNumbers = new List<PhoneNumber>();
            addContact.Tags = new List<Tag>();
            addContact.Emails.Add(new Email { EmailAddress = "belacic.goran@gmail.com" });
            addContact.PhoneNumbers.Add(new PhoneNumber { Number = "0989131219" });
            addContact.Tags.Add(new Tag { TagName = "hrcloud" });
            addContact.Tags.Add(new Tag { TagName = "AngularJS" });

            context.Contacts.AddOrUpdate(addContact);

            Contact test = new Contact();
            test.FirstName = "Mia";
            test.LastName = "Jelic";
            test.City = "Split";
            test.Address = "Put mora";
            test.IsBookmarked = false;
            test.Emails = new List<Email>();
            test.PhoneNumbers = new List<PhoneNumber>();
            test.Tags = new List<Tag>();
            test.Emails.Add(new Email { EmailAddress = "mia.jelic@gmail.com" });
            test.Emails.Add(new Email { EmailAddress = "mia.jelic@yahoo.com" });
            test.PhoneNumbers.Add(new PhoneNumber { Number = "0951457852" });
            test.Tags.Add(new Tag { TagName = "more" });
            test.Tags.Add(new Tag { TagName = "MVC" });

            context.Contacts.AddOrUpdate(test);

            Contact testUser = new Contact();
            testUser.FirstName = "Dominik";
            testUser.LastName = "Radic";
            testUser.City = "Zagreb";
            testUser.Address = "Strojarska";
            testUser.IsBookmarked = false;
            testUser.Emails = new List<Email>();
            testUser.PhoneNumbers = new List<PhoneNumber>();
            testUser.Tags = new List<Tag>();
            testUser.Emails.Add(new Email { EmailAddress = "dominik.radic@fer.hr" });
            testUser.Emails.Add(new Email { EmailAddress = "domi10@fmail.com" });
            testUser.PhoneNumbers.Add(new PhoneNumber { Number = "0951453332" });
            testUser.PhoneNumbers.Add(new PhoneNumber { Number = "0951166128" });
            testUser.Tags.Add(new Tag { TagName = "student" });
            testUser.Tags.Add(new Tag { TagName = "tag" });

            context.Contacts.AddOrUpdate(testUser);

            Contact deletedUser = new Contact();
            deletedUser.FirstName = "Marko";
            deletedUser.LastName = "Horvat";
            deletedUser.City = "Zagreb";
            deletedUser.Address = "Strojarska";
            deletedUser.IsBookmarked = false;
            deletedUser.isDeleted = true;
            deletedUser.Emails = new List<Email>();
            deletedUser.PhoneNumbers = new List<PhoneNumber>();
            deletedUser.Tags = new List<Tag>();
            deletedUser.Emails.Add(new Email { EmailAddress = "marko.horvat@gmail.hr" });
            deletedUser.Emails.Add(new Email { EmailAddress = "marko@mail.com" });
            deletedUser.PhoneNumbers.Add(new PhoneNumber { Number = "0951453999" });
            deletedUser.PhoneNumbers.Add(new PhoneNumber { Number = "0951453977" });
            deletedUser.Tags.Add(new Tag { TagName = "deleted" });
            deletedUser.Tags.Add(new Tag { TagName = "contact" });

            context.Contacts.AddOrUpdate(deletedUser);





        }
    }
}
