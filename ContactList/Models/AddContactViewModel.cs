﻿using System.Collections.Generic;

namespace ContactList.Models
{
    public class AddContactViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public string Address { get; set; }        
        public bool IsBookmarked { get; set; }
        public List<string> Numbers { get; set; }
        public List<string> Emails { get; set; }
        public List<string> Tags { get; set; }
    }
}