﻿using System.Collections.Generic;

namespace ContactList.Models
{
    public class EditContactViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
       
        public bool IsBookmarked { get; set; }
        public bool IsDeleted { get; set; }
        public List<PhoneNumber> Numbers { get; set; }
        public List<Email> Emails { get; set; }
        public List<Tag> Tags { get; set; }
    }
}