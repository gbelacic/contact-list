﻿using System.Collections.Generic;

namespace ContactList.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string TagName { get; set; }
        
        public virtual List<Contact> Contacts { get; set; }
    }
}